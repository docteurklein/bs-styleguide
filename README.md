# bs-less-styleguide

## what ?

A gulp skeleton to help the creation of semantic html snippets,
styled using bootstrap mixins.

## how ?

    npm install
    gulp

Then, modify or add some files in:

 - `html/*`
 - `less/*`
 - `sass/*`
 - `index.html`


A server is listening at http://localhost:8080
