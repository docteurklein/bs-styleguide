var gulp = require('gulp');
var less = require('gulp-less');
var sass = require('gulp-sass');
var path = require('path');
var http = require('http');
var st = require('st');
var concat = require('gulp-concat');
var template = require('gulp-template');
var data = require('gulp-data');

gulp.task('html', function() {
  gulp.src('./html/**/*.html')
    .pipe(concat('snippets.html'))
    .pipe(data(function(file) {
      gulp.src('./index.html')
        .pipe(template({content: String(file.contents)}))
        .pipe(gulp.dest('./dist'))
      ;
    }))
    .pipe(gulp.dest('./dist'))
  ;
});

gulp.task('less', function () {
  gulp.src('./less/**/*.less')
    .pipe(less())
    .pipe(gulp.dest('./dist/css/less'))
  ;
});

gulp.task('sass', function () {
  gulp.src('./sass/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./dist/css/sass'))
  ;
});

gulp.task('watch', ['server', 'html', 'less', 'sass'], function() {
  gulp.watch('less/**/*.less', ['less']);
  gulp.watch('sass/**/*.scss', ['sass']);
  gulp.watch('html/**/*.html', ['html']);
  gulp.watch('./index.html', ['html']);
});

gulp.task('server', function(done) {
  http.createServer(
    st({ path: __dirname + '/dist', index: 'index.html', cache: false })
  ).listen(8080, done);
});

gulp.task('default', ['watch']);
